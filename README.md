# H-Viewer Sites
All sites for H-Viewer

## 2020年5月23日

- [ ] xgmn的显示总是显示盗链的问题解决。 解决不了，单纯浏览器打开都会出现这个问题，让我怎么解决。
- [ ] 查看mzitu的盗链问题是不是也是这个问题
- [x] taotu55网站编辑,基本完成了。
- [x] 接下来就是taotu55的categories完备化，python爬取


## 2020年5月18日

- [ ] www.ssmh.cc 网站编辑漫画作为练手
- [ ] 漫画规则真的不简单，还是说只是这个网站不简单啊。他很多都是检查和Ctrl+u不一样的，这个还好，按照ctrl+u的标准来就行了。关键是为啥有些图片的地址在后面的上面是没有的呢 #这个问题好像解决不了，就是这个网站就是设计出来只能一个页面的那种
- [ ] 现在没有解决的问题是第一个多个章节的话，只能显示第一个章节的内容，第二个，搜索出来的页面图片不配对。其实这个可以通过js加载基本上没有问题了，可是本来用这个来看图片的话已经很慢了，再加上这个的话会更加慢的。这也说明这个软件其实是针对的是两个页面来设计的，对于三个页面的话好像也没有想这么多。
- [ ] 在编写的过程中，发现电脑上浏览器直接打开的话会出现问题的，关键在于不能用浏览器直接打开，然后伪装一下华为手机的ua基本上也就解决了。但是发现竟然在爬取测试和手机上运行的时候都不需要添加header也是没有问题。



## 2020年5月17日

1. [x] h-bilibili改为bilibili # ok
2. [x] bilibili画友全部最新改名 # 还是把最热的改为第一个
3. [x] 增添xiurenji到json # ok
4. [x] bilibili添加onepicture的flag # 只是需要打开设置中单图直接浏览即可生效
5. [x] 添加mzitu的搜索 # ok 测试通过
6. [x] xiurenji.json的categories爬取一下 #基本上解决了，就是这个tags会有点问题
7. [x] Bilibili 文件改名小写，然后看看有没有recommand界面 # 仅仅改了一下indexUrlde recommend不知道行不行
8. [x] xiurenji.json的日期改一下 

----

9. [ ] bilibili画友看看能不能提取网站评论，感觉有点难 # 发现问题，git不区分大小写，我在找解决方案的时候最后屈服了，直接改为大写的
10. [ ] 看看那个图标为啥不显示
11. [x] www.nvshens修改搜索看看 #不容易修改，放弃了

12. [x] www.quanjixiu.top网站的编写，里面一个xgmn好了

----

13. [ ] 看看漫画到底是怎么编辑的
14. [x] 177漫画已经完成
15. [ ] www.mumumh.com的编辑 #这个网站502了，编着编着就502了。本来想用这个练练手，漫画的编辑的。secondLevel

## 2020年5月15日

1. 爬取堆糖的网页，关键实现评论的获取
2. ui中国的图片尝试获取js格式的
3. 看看Fillder

## 2020年5月14日

1. Bilbili画友已经是通过js加载了，无法用这个软件爬取，其实可以删除txt文件了
2. 从“扒图”软件上看到[宅男猫](http://www/zhainanmao.la)网站，发现他这个第一页[软妹子](http://www.zhainanmao.la/ruanmeizi)和后面的不一样。但是这个网站结构简单，而且是静态的网页，第一页非常容易抓取。等会后面的网页抓取试试。
3. mntp01网站好像炸了，感觉这种网站真的很容易炸的啊。

### 准备完成
1. 修改json，添加zhaonanmao网址
2. 编辑zhaonanmao后面网址

## 2020年5月10日

### 已经完成

0. "versionCode"=1
1. 已经完成修复妹子图网站，修改名称为mzitu.txt.
2. 添加mntp01网站。
3. 修复宅男女神网站，修改名称为nvshens.txt
4. 今天尝试修复avmoo网站，失败。他好像禁止爬虫之类的，几页访问出错就自动无法访问。

### 准备完成

- 完成mntp的tag，优先级比较低 -完成
- 同样添加mntp的时间，可以的话加上tag -完成，还加上了一个description
- nvshens的date格式修复 -完成，顺便修复tags
- git push上去



注：这个列表已不再更新，获取最新最全的站点请在应用内的站点市场

## Sites [_](https://github.com/H-Viewer-Sites/Index/blob/master/HIDDEN.md)


### ACG图站 —— ACG picture sites  

|  ID  | Site's Json  | QR Code | Author |
| ---- | ------------- | ------------- | ------------- |
|  1   | [E-shuushuu](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/E-shuushuu.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/E-shuushuu.png)  | PureDark |
|  2   | [Pixiv](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/Pixiv.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/Pixiv.png)  | PureDark <br> & <br> [manhong2112](https://github.com/manhong2112) |


### Booru图站 —— Booru sites
#### Post是单张图，Pool是图册，上过booru的朋友都明白

|  ID  | Site's Json  | QR Code | Author |
| ---- | ------------- | ------------- | ------------- |
|  1   | [Konachan Post](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/Konachan.Post.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/Konachan.Post.png)  | PureDark |
|  2   | [Konachan Pool](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/Konachan.Pool.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/Konachan.Pool.png)  | PureDark |
|  3   | [3dbooru Post](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/3dbooru.Post.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/3dbooru.Post.png)  | PureDark |
|  4   | [3dbooru Pool](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/3dbooru.Pool.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/3dbooru.Pool.png)  | PureDark |


### 写真图站  —— Reality Sites

|  ID  | Site's Json  | QR Code | Author |
| ---- | ------------- | ------------- | ------------- |
|  1   | [秀人](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/秀人.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/秀人.png)  | lbq from v2ex |
|  2   | [Rosi](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/Rosi.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/Rosi.png)  | [vvyoko](https://github.com/vvyoko) |
|  3   | [绝对领域](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/sites/绝对领域.txt) | ![](https://raw.githubusercontent.com/H-Viewer-Sites/Index/master/images/绝对领域.png)  | PureDark |
