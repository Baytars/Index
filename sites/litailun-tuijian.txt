﻿{
    "title": "litailun - 推荐",
    "flag": "preloadGallery|noRating",
    "categories": [
        {
            "cid": 1,
            "title": "推荐",
            "url": "http://litailun.com/Tuijian/index{pageStr:/p/{page:1}.html}"
        }
    ],
    "indexUrl": "http://litailun.com/Tuijian/index{pageStr:/p/{page:1}.html}",
    "indexRule": {
        "item": {
            "selector": "ul.bqul>li"
        },
        "cover": {
            "selector": "a>img",
            "fun": "attr",
            "param": "src"
        },
        "idCode": {
            "selector": "a",
            "fun": "attr",
            "param": "href",
            "regex": "//(\\d+\\.html)",
            "replacement": "http://litailun.com/Tuijian/index/$1"
        },
        "title": {
            "selector": "span",
            "fun": "text"
        }
    },
    "galleryUrl": "http://litailun.com/Tuijian/index{pageStr:/p/{page:1}.html}",
    "galleryRule": {
        "description": {
            "selector": "div.bqbox a[title]",
            "fun": "attr",
            "param": "title"
        },
        "tags": {
            "selector": "meta[name='keywords']",
            "fun": "attr",
            "param": "content",
            "regex": "([^,]+)"
        },
        "pictureRule": {
            "item": {
                "selector": "ul.bqul>li>a>img"
            },
            "thumbnail": {
                "selector": "img",
                "fun": "attr",
                "param": "src"
            },
            "url": {
                "selector": "img",
                "fun": "attr",
                "param": "src"
            }
        }
    },
    "versionCode": 1
}
